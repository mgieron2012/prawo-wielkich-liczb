# Prawo wielkich liczb


## Zadanie

Proszę przeprowadzić symulację doświadczeń w schemacie Bernoulliego (np. rzut kostką, monetą niesymetryczną, itp. - nie może to być rzut monetą symetryczną) dla n=50, 100, 500, 1000, 1500, 2000, 2500. W każdym przypadku obliczyć częstość wystąpienia sukcesu. Narysować wykres obrazujący zależność częstości sukcesu od liczby doświadczeń. Skomentować, jak ma się częstość sukcesu w stosunku do prawdopodobieństwa sukcesu w jednej próbie.

## Odpowiedzi

Badano częstotliwość wyrzucenia trzech oczek przy rzucie symetryczną sześcienną kostką do gry. 

Prawdopodobieństwo zdarzenia: 1/6.

![Wykres](plot.png)

Widać, że dla prób o rozmiarze większym równym 500 szacowane prawdopodobieństwo jest bardzo bliskie 1/6. Ponieważ bazujemy na losowości, nie ma gwarancji, że zawsze dla coraz większej próby szacowane prawdopodobieństwo będzie coraz dokładniejsze. Możliwe jest, że dla sześciu prób wypadnie jedna trójka, a tysiąca prób nie wypadnie ani razu. Jednak w większości przypadków dla dużych liczb szacowane prawdopodobieństwo będzie bardzo dokładne.
