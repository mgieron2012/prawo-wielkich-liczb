import random
import matplotlib.pyplot as plt

sizes = [50, 100, 500, 1000, 1500, 2000, 2500, 10000]
probability = 1/6

results = []

for size in sizes:
    rolls = random.choices([1, 2, 3, 4, 5, 6], k=size)
    results.append(rolls.count(3) / size)

fig, ax = plt.subplots()
p1 = ax.bar(range(8), results, 0.85)
ax.axhline(probability, color='grey', linewidth=1)
ax.set_xticks(range(8), labels=sizes)
plt.show()
